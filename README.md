# README Face Recognition#

### Language ###

PHP 5.5

### IDE ###

NetBeans 8.0

### Get Starded ###

**Include class file and helper file**


```
#!php

include('Kairos.php');
include('helper.php');
```

**Set your API Key & ID**

```
#!php

$app_id  = '30226d34';
$api_key = 'f59834cbc0b0388ae858b4ae79262bb0';
```

**Create new instance for authentification**

```
#!php

$Kairos  = new Kairos($app_id, $api_key);
```

**Add image in gallery model**


```
#!php

$gallery_id = 'RecoFacial';
$subject_id = 'Leonardo';
$image_path = 'leonardo.jpg';
$response = $Kairos->enrollImageWithPath($image_path, $gallery_id, $subject_id);
```

**Match image sent with gallery's image**


```
#!php

$path       = 'leonardo3.jpg';
$option     = array('threshold' => 0.70);
$response   = $Kairos->recognizeImageWithPath($path, $gallery_id, $option);
```

###Display JSON Response###

Function "format_json" found in helper.php
```
#!php
echo format_json($response, true);
```

###To Do###

* Kairos linked with the rest of the application