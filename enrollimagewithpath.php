<html>
<head>
<link rel="stylesheet" href="css/bootstrap.min.css" />
</head>
<body>
<h2>Enrolling an Image Using a File Path</h2>
<i>This example enrolls the following image</i><br />
<img src="leonardo2.jpeg" />
<?php

//* * * * include the wrapper class
include('Kairos.php');
include('helper.php');

//* * * * sample api credentials (works for example)
$app_id  = '30226d34';
$api_key = 'f59834cbc0b0388ae858b4ae79262bb0';

//* * * * create a new instance and authenticate
$Kairos  = new Kairos($app_id, $api_key);

/*----------------------Permet l'enregistrement de modèle photo dans une gallery----------------------*/
$gallery_id = 'RecoFacial';
$subject_id = 'JackBlack';
$image_path = 'leonardo_barbu.jpg';
$response = $Kairos->enrollImageWithPath($image_path, $gallery_id, $subject_id);

var_dump($response);

echo '<br /><b>Response:</b><br />';
echo '<div class="text-left" style="padding:10px;border:1px solid rgba(51, 51, 51, 0.08);background-color: rgba(204, 204, 204, 0.26);"><br />';
echo format_json($response, true);
echo '</div>';
/*--------------------------------------------*/

/*----------------------Pour supprimer la gallery----------------------*/
$response   = $Kairos->removeGallery($gallery_id);
$response = $Kairos->listGalleries();
var_dump($response);
/*--------------------------------------------*/

/*----------------------Permet le match entre la photo et la gallery----------------------*/
$path       = 'leonardo3.jpg';
$option     = array('threshold' => 0.70);
$response   = $Kairos->recognizeImageWithPath($path, $gallery_id, $option);
echo "Resultat du RecognizeImage: ";
var_dump($response);
echo '<br /><b>Response:</b><br />';
echo '<div class="text-left" style="padding:10px;border:1px solid rgba(51, 51, 51, 0.08);background-color: rgba(204, 204, 204, 0.26);"><br />';
echo format_json($response, true);
echo '</div>';
/*--------------------------------------------*/

?>
</body>
</html>